"""Cost functions."""

import abc
import numpy as np
from .core import BaseOptimiser, BaseTarget


class IncoherentSumCostOptimiser(BaseOptimiser):
    def cost(self, values):
        """Compute the result of the cost function given the specified free parameter values."""
        # Least squares. Take the square root so optimiser tolerance doesn't need to be so small.
        return np.sqrt(sum([parameter.cost() ** 2 for parameter in self.targets]))


class LinearCostTarget(BaseTarget, metaclass=abc.ABCMeta):
    def cost(self):
        return self.value - self.target
