"""Core optimisation functions."""

import abc
from dataclasses import dataclass
from functools import reduce
from finesse.utilities import ngettext


@dataclass
class OptimiseResult:
    """Optimisation result container."""

    success: bool
    message: str
    niterations: int


class BaseOptimiser(metaclass=abc.ABCMeta):
    """Base optimiser."""

    def __init__(self):
        self.variables = []
        self.targets = []
        self._optimise_result = None

    def allow(self, variable):
        """Set a model parameter to vary."""
        self.variables.append(variable)

    def target(self, target):
        """Set a model parameter to target."""
        self.targets.append(target)

    def optimise(self, **kwargs):
        result = self._do_optimise(**kwargs)

        self._optimise_result = self._parse_optimise_result(result)

        if self.optimisation_successful:
            self._on_success()

    @abc.abstractmethod
    def _parse_optimise_result(self, result):
        raise NotImplementedError

    @property
    def optimisation_successful(self):
        return self._optimise_result.success

    @property
    def niterations(self):
        return self._optimise_result.niterations

    @property
    def optimisation_message(self):
        return self._optimise_result.message

    @abc.abstractmethod
    def _do_optimise(self, **kwargs):
        raise NotImplementedError

    def make_variable_table(self, tablefmt="fancy_grid"):
        """Construct table showing variables and their values.

        Parameters
        ----------
        tablefmt : str, optional; default: "fancy_grid"
            The plain-text table format to pass to tabulate.

        Returns
        -------
        str
            The table as a string.
        """
        from tabulate import tabulate

        table = []
        headers = ["Parameter", "Initial value", "Current value"]

        for param in self.variables:
            table.append(
                [
                    param,
                    f"{param.initial_value:.2e}",
                    f"{param.value:.2e} ({-100 * param.difference():.2f}%)",
                ]
            )

        return tabulate(table, headers, tablefmt=tablefmt)

    def make_target_table(self, tablefmt="fancy_grid"):
        """Construct table showing targets and their values.

        Parameters
        ----------
        tablefmt : str, optional; default: "fancy_grid"
            The plain-text table format to pass to tabulate.

        Returns
        -------
        str
            The table as a string.
        """
        from tabulate import tabulate

        table = []
        headers = [
            "Parameter",
            "Target value",
            "Initial value",
            "Current value",
        ]

        for param in self.targets:
            table.append(
                [
                    param,
                    f"{param.target:.2e}",
                    f"{param.initial_value:.2e} ({-100 * param.difference():.2f}%)",
                    f"{param.value:.2e} ({-100 * param.target_difference():.2f}%)",
                ]
            )

        return tabulate(table, headers, tablefmt=tablefmt)

    def info(self, tablefmt="fancy_grid"):
        """Get optimisation information.

        Parameters
        ----------
        tablefmt : str, optional; default: "fancy_grid"
            The plain-text table format to pass to tabulate.

        Returns
        -------
        str
            The table as a string.
        """
        strinfo = f"Optimisation of {self.model!r}"

        if self.is_optimised:
            ntxt = ngettext(self.niterations, "%d iteration", "%d iterations")
            strinfo += f" finished in {ntxt} ({self.optimisation_message})"
        elif self._optimise_result is not None:
            strinfo += f" (could not find optimum: {self.optimisation_message})"
        else:
            strinfo += f" (optimisation not yet attempted)"

        strinfo += "\n\n"
        strinfo += "Targets:\n"
        strinfo += self.make_target_table(tablefmt=tablefmt)

        strinfo += "\n\n"
        strinfo += "Variables:\n"
        strinfo += self.make_variable_table(tablefmt=tablefmt)

        return strinfo

    def print(self):
        """Print optimisation information."""
        print(str(self))

    @property
    def is_optimised(self):
        return self._optimise_result is not None and self.optimisation_successful

    def __str__(self):
        return self.info()

    @abc.abstractmethod
    def cost(self, values):
        """Compute the result of the cost function given the specified free parameter values."""
        raise NotImplementedError

    @property
    def x(self):
        """Current variable values."""
        return {parameter: parameter.value for parameter in self.variables}

    def _on_success(self):
        """Hook called when the optimisation is successful."""
        pass


class BaseQuantity(metaclass=abc.ABCMeta):
    """Base class for all quantities that can be get or set during the optimisation."""

    def __init__(self):
        self.initial_value = self.value

    def difference(self):
        """Compute the difference between the current and initial value for this parameter."""
        return 1 - self.value / self.initial_value

    @property
    @abc.abstractmethod
    def value(self):
        raise NotImplementedError

    @value.setter
    @abc.abstractmethod
    def value(self, value):
        raise NotImplementedError

    def __str__(self):
        return f"{self.__class__.__name__}"


class BaseTarget(BaseQuantity, metaclass=abc.ABCMeta):
    """Base class for targets."""

    def __init__(self, target, **kwargs):
        self.target = target
        super().__init__(**kwargs)

    def target_difference(self):
        """Compute the difference between the current and target value for this parameter."""
        return 1 - self.value / self.target

    @abc.abstractmethod
    def cost(self, value):
        raise NotImplementedError

    def __str__(self):
        return f"{super().__str__()}->{self.target}"


class AttributeQuantity(BaseQuantity, metaclass=abc.ABCMeta):
    """A quantity taken from an object's attribute.

    Parameters
    ----------
    item : any
        The item that possesses the attribute.

    attribute : str or sequence
        An attribute of `item` as a string, such as "P" or "p1.o.q.w", or a sequence of strings such
        as `["p1", "o", "q", "w"]`.
    """

    def __init__(self, item, attribute, **kwargs):
        # TODO ssl: allow real model parameter objects to be passed too.
        self.item = item

        if isinstance(attribute, str):
            attribute = attribute.split(".")
        self.attributes = list(attribute)

        self.initial_value = self.value

        super().__init__(**kwargs)

    @property
    def value(self):
        value = self._nested_get([self.item] + self.attributes)

        if callable(value):
            value = value()

        try:
            value = value.value
        except AttributeError:
            pass

        return value

    @value.setter
    def value(self, value):
        setattr(
            self._nested_get([self.item] + self.attributes[:-1]),
            self.attributes[-1],
            value,
        )

    @staticmethod
    def _nested_get(attributes):
        return reduce(getattr, attributes)

    def __str__(self):
        return f"{self.item!r}.{'.'.join(self.attributes)}"
