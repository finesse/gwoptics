"""Bounded optimisation functions."""

import abc
from .core import BaseOptimiser, BaseQuantity, AttributeQuantity, OptimiseResult


class BoundedOptimiser(BaseOptimiser, metaclass=abc.ABCMeta):
    """An optimiser with support for bounded variables.

    Concrete classes should implement a `cost` function.

    Parameters
    ----------
    model : :class:`.Model`
        The model to optimise.
    """

    def _do_optimise(self, **kwargs):
        """Minimise difference between targets and corresponding parameter values by varying free \
        parameters.

        Other Parameters
        ----------------
        tol : float, optional
            Tolerance for termination.

        options : dict, optional
            Solver-specific options.
        """
        from scipy.optimize import minimize

        return minimize(
            self._do_cost,
            x0=list(self.x.values()),
            bounds=list(self.bounds.values()),
            **kwargs,
        )

    @property
    def bounds(self):
        """Variable bounds."""
        return {
            parameter: parameter.bounds
            for parameter in self.variables
            if isinstance(parameter, BoundedQuantity)
        }

    def _do_cost(self, values):
        # Update variables.
        self._update_variables(values)

        # Run callback.
        self._pre_cost(values)

        return self.cost(values)

    def _update_variables(self, values):
        for variable, value in zip(self.variables, values):
            variable.value = value

    def _parse_optimise_result(self, result):
        return OptimiseResult(
            success=result.success, message=result.message, niterations=result.nit
        )

    @abc.abstractmethod
    def _pre_cost(self, values):
        """Hook run before computing cost function."""
        raise NotImplementedError


class DerivativeFreeBoundedOptimiser(BoundedOptimiser, metaclass=abc.ABCMeta):
    def _do_optimise(self, **kwargs):
        """Minimise difference between targets and corresponding parameter values by varying free \
        parameters.

        Other Parameters
        ----------------
        tol : float, optional
            Tolerance for termination.

        options : dict, optional
            Solver-specific options.
        """
        from pybobyqa import solve

        # Py-BOBYQA takes bounds as (lower, upper) where lower and upper are vectors of length x0,
        # in contrast to scipy.optimize.minimize which takes a sequence of length x0 of
        # (lower, upper) tuples.
        bounds = list(map(list, zip(*self.bounds.values())))

        return solve(self._do_cost, x0=list(self.x.values()), bounds=bounds, **kwargs,)

    def _parse_optimise_result(self, result):
        return OptimiseResult(
            success=result.flag == result.EXIT_SUCCESS,
            message=result.msg,
            niterations=result.nf,
        )


class BoundedQuantity(BaseQuantity):
    """A quantity with bounds."""

    def __init__(self, bounds, **kwargs):
        self.bounds = bounds
        super().__init__(**kwargs)

    def __str__(self):
        return f"{super().__str__()}->{self.bounds}"


class BoundedAttributeQuantity(AttributeQuantity, BoundedQuantity):
    """An attribute lying within an interval.

    Parameters
    ----------
    item : any
        The item that possesses the attribute.

    attribute : str or sequence
        An attribute of `item` as a string, such as "P" or "p1.o.q.w", or a sequence of strings such
        as `["p1", "o", "q", "w"]`.

    bounds : tuple or array like
        Two element tuple or other iterable defining the boundaries of the free parameter space to
        explore.
    """
