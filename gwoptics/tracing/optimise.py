"""Optimiser for geometric properties of optical components in a model."""

from ..optimise.core import AttributeQuantity
from ..optimise.bounded import (
    DerivativeFreeBoundedOptimiser,
    BoundedAttributeQuantity,
    BoundedQuantity,
)
from ..optimise.cost import IncoherentSumCostOptimiser, LinearCostTarget


class LinearAttributeQuantity(AttributeQuantity, LinearCostTarget):
    """Attribute quantity with linear cost function."""


class TraceOptimiser(DerivativeFreeBoundedOptimiser, IncoherentSumCostOptimiser):
    """Model beam trace optimiser using least squares."""

    def __init__(self, model):
        super().__init__()
        self.model = model

        # Perform an initial beam trace so that beam parameters are set at each node and are
        # therefore available to the user to set as free/target parameters.
        self.model.beam_trace()

    def _pre_cost(self, values):
        # Trace the model with updated values.
        self.model.beam_trace()


class BoundedComponentPosition(BoundedQuantity):
    """Parameter representing the average length of a pair of spaces."""

    def __init__(self, item, **kwargs):
        self.item = item
        super().__init__(**kwargs)

    @property
    def space1(self):
        return self.item.p1.i.space

    @property
    def space2(self):
        return self.item.p2.o.space

    @property
    def value(self):
        return float(self.space1.L)

    @value.setter
    def value(self, length):
        dl = length - self.space1.L
        self.space1.L = length
        self.space2.L -= dl


ALL = (
    TraceOptimiser,
    LinearAttributeQuantity,
    BoundedAttributeQuantity,
)
