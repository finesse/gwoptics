from setuptools import setup, find_packages

with open("README.md") as readme_file:
    README = readme_file.read()

REQUIREMENTS = [
    "tabulate",
]

EXTRAS = {
    "optimise": [
        "Py-BOBYQA",
    ]
}

setup(
    name="gwoptics",
    use_scm_version={
        "write_to": "gwoptics/_version.py"
    },
    description="Libraries and tools for gravitational wave interferometry and optics.",
    long_description=README,
    url="https://git.ligo.org/finesse/gwoptics",
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    extras_require=EXTRAS,
    setup_requires=["setuptools_scm"],
    license="GPLv3",
)
