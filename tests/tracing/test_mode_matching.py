"""Mode matching optimiser tests."""

import pytest
from finesse.gaussian import BeamParam
from finesse.components import Laser, Lens, Mirror
from gwoptics.tracing.optimise import (
    TraceOptimiser,
    BoundedAttributeQuantity,
    LinearAttributeQuantity,
)


@pytest.fixture
def telescope_optimiser(model):
    model.chain(
        Laser("lsr"),
        1,
        Lens("l1", f=-1),
        1,
        Lens("l2", f=1),
        1,
        Mirror("m1", Rc=-15),
        10,
        Mirror("m2", Rc=15),
    )

    # Set the laser parameters.
    model.lsr.p1.o.q = BeamParam(w0=370e-6, z=-0.09)

    return TraceOptimiser(model)


def test_telescope_position_optimisation(telescope_optimiser):
    """Test target beam size optimisation with two lenses of equal and opposite focal lengths."""
    model = telescope_optimiser.model

    # Set parameters that can be varied.
    telescope_optimiser.allow(
        BoundedAttributeQuantity(
            item=model.lsr, attribute="p1.o.space.L", bounds=(0.1, 1.5)
        )
    )
    telescope_optimiser.allow(
        BoundedAttributeQuantity(
            item=model.l1, attribute="p2.o.space.L", bounds=(0.1, 1.5)
        )
    )

    # Set target to optimise mode matching.
    telescope_optimiser.target(
        LinearAttributeQuantity(model.m1, "p2.o.qx.w", target=1.9e-3)
    )

    # Solve.
    telescope_optimiser.optimise()

    assert telescope_optimiser.is_optimised

    # Check the beam is now close to the target.
    for param in telescope_optimiser.targets:
        assert abs(param.target_difference()) <= 1e-5
