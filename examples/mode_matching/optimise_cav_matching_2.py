"""Demonstrate the optimiser for a target cavity beam size by varing lens position.

See optimise_cav_matching_1.py for an example varying space lengths instead.

Sean Leavey  
<sean.leavey@ligo.org>
"""

from finesse import Model, BeamParam
from finesse.components import Cavity, Laser, Lens, Mirror
from gwoptics.tracing.optimise import (
    TraceOptimiser,
    BoundedComponentPosition,
    LinearAttributeQuantity,
)


# Make a model with only the cavity mirrors so we can first compute the ideal case.
model = Model()
model.chain(
    Mirror("m1", Rc=-15), 10, Mirror("m2", Rc=15),
)
cav = Cavity("cav", model.m1.p2.o)
model.add(cav)
model.beam_trace()

print(f"cav waist radius: {model.cav.w0 * 1e3} mm")
print(f"m2 beam radius: {model.m2.p1.i.qx.w*1e3:.2f} mm (x), {model.m2.p1.i.qy.w*1e3:.2f} (y)")
ts = model.propagate_beam(model.m1.p2.o, model.m2.p1.i, symbolic=True)
print(ts)
ts.plot()

# Save the beam size at the cavity mirrors to set as a target later.
target_beam_w = model.m2.p1.i.qx.w

# Remove the cavity so that we can mode match the real laser to the cavity.
model.remove(cav)

# Add the input path.
model.chain(Laser("lsr"), 1, Lens("l1", f=-1), 1, Lens("l2", f=1))
model.connect(model.l2.p2, model.m1.p1, L=1)

# Set the real laser beam (typical NPRO).
model.lsr.p1.o.q = BeamParam(w0=370e-6, z=-0.09)

# Create an optimiser for the model.
optimiser = TraceOptimiser(model=model)

# Allow the spaces between the lenses to vary.
optimiser.allow(BoundedComponentPosition(item=model.l1, bounds=(0.1, 5)))

# Set target beam size at end mirror.
optimiser.target(LinearAttributeQuantity(model.m1, "p2.o.qx.w", target=target_beam_w))
optimiser.target(LinearAttributeQuantity(model.m2, "p1.i.qx.w", target=target_beam_w))

# optimiser.optimise(tol=1e-10)  # For use with derivative optimiser
optimiser.optimise(rhoend=1e-10)

print("\nAfter solving:\n")
print(optimiser)

ts = model.propagate_beam(model.lsr.p1.o, model.m2.p1.i, symbolic=True)
print(ts)
ts.plot()
