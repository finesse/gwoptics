"""Beam parameter guessing for Enhanced LIGO front-end laser.

Measurements taken by Sean Leavey at the AEI on an Enhanced LIGO front-end MOPA laser, using a
WinCam. Beam sizes are read from the WinCam software and are probably not optimal. Positions were
eyeballed and could be wrong by up to around +/-3 cm.

Optimisation of the lens focal length shows it has twice the focal length in reality than the
MOPA manual suggests.

Sean Leavey  
<sean.leavey@ligo.org>
"""

from finesse import Model, BeamParam
from finesse.components import Laser, Lens, Mirror, Nothing
from gwoptics.tracing.optimise import (
    TraceOptimiser,
    BoundedAttributeQuantity,
    LinearAttributeQuantity,
)

model = Model()
model.chain(
    Laser("lsr"),
    0.115,
    Lens("l1", f=0.1),  # From MOPA manual.
    0.145,
    Mirror("meomfr"),
    {"L": 30e-3, "nr": 2.3},  # Lithium niobate.
    Mirror("meombk"),
    0.285,
    Nothing("cam1"),
    0.037,
    Nothing("cam2"),
    0.068,
    Nothing("cam5"),
    0.020,
    Nothing("cam6"),
)

# Set beam waist to be at the laser.
model.lsr.p1.o.q = BeamParam(w0=370e-6, z=-90e-3)

# Create optimiser.
optimiser = TraceOptimiser(model)

# Set parameters that can be varied.
optimiser.allow(
    BoundedAttributeQuantity(item=model.l1, attribute="f", bounds=(0.05, 0.3))
)

# Set target beam size at mirrors beyond lenses.
optimiser.target(LinearAttributeQuantity(model.cam1, "p1.i.qx.w", target=550e-6))
optimiser.target(LinearAttributeQuantity(model.cam2, "p1.i.qx.w", target=680e-6))
optimiser.target(LinearAttributeQuantity(model.cam5, "p1.i.qx.w", target=811.2e-6))
optimiser.target(LinearAttributeQuantity(model.cam6, "p1.i.qx.w", target=858.8e-6))

# Solve.
optimiser.optimise()

print(optimiser)

# Check final beam matches targets.
print(f"cam1 w = {model.cam1.p1.i.qx.w*1e6} µm")
print(f"cam2 w = {model.cam2.p1.i.qx.w*1e6} µm")
print(f"cam5 w = {model.cam5.p1.i.qx.w*1e6} µm")
print(f"cam6 w = {model.cam6.p1.i.qx.w*1e6} µm")
# Print the lens focal length that was found.
print(f"l1 f = {model.l1.f}")

ts = model.propagate_beam(model.lsr.p1.o, model.cam6.p1.i)
print(ts)
ts.plot()
